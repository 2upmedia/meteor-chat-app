import { Accounts } from 'meteor/accounts-base'
import { Meteor } from 'meteor/meteor'
import { Mongo } from 'meteor/mongo'

LiveUsers = new Mongo.Collection('liveUsers')
ChatMessages = new Mongo.Collection('chatMessages')

Accounts.ui.config({
  passwordSignupFields: "USERNAME_ONLY"
});

Accounts.onLogout(function() {
  Meteor.call('loggedOut', Meteor.userId())
});

var chatModule = angular
  .module('chat-app',['angular-meteor', 'accounts.ui'])

chatModule.service('ChatService', ['$meteor', function ($meteor) {
    return {
      liveUsersCollection: $meteor.collection(function() {
        return LiveUsers.find()
      }),
      messagesCollection: $meteor.collection(function() {
        return ChatMessages.find({}, {sort: { createdAt: -1}})
      }),
      addChatMessage: function (message) {
        $meteor.call('addChatMessage', message)
      }
    }
  }])

chatModule.controller('ChatListCtrl', ['$scope', '$meteor', 'ChatService',
  function ($scope, $meteor, ChatService) {
    $scope.$meteorSubscribe('liveUsers')
    $scope.$meteorSubscribe('chatMessages')

    $scope.liveUsers = ChatService.liveUsersCollection
    $scope.messages = ChatService.messagesCollection

    $scope.addChatMessage = ChatService.addChatMessage

  }])

angular.element(document).ready(() => angular.bootstrap(document, ['chat-app']))