import { Meteor } from 'meteor/meteor'
import { Mongo } from 'meteor/mongo'

Tasks = new Mongo.Collection('tasks')
LiveUsers = new Mongo.Collection('liveUsers')
ChatMessages = new Mongo.Collection('chatMessages')

Meteor.publish("liveUsers", function () {
  var id = this.userId
  var user = getUser(id)

  if (user) {
    LiveUsers.upsert({username: user.username}, user)
  }

  this._session.socket.on(
    "close",
    Meteor.bindEnvironment(
      function () {
        if (user) {
          removeLiveUser(user)
        }
      }
    )
  )

  return LiveUsers.find({})
});

Meteor.publish("chatMessages", function () {
  if (this.userId) {
    return ChatMessages.find()
  } else {
    this.ready()
  }
})

function removeLiveUser(user) {
  LiveUsers.remove({ username: user.username })
}

function getUser(userId) {
  return Meteor.users.findOne({ _id: userId }, { fields: { username: 1 } })
}

Meteor.methods({
  loggedOut: function (userId) {
    var user = getUser(userId)

    if (user) {
      removeLiveUser(user)
    }
  },
  addChatMessage: function (message) {
    var user = Meteor.user()

    ChatMessages.insert({
      text: message,
      createdAt: new Date(),
      owner: user._id,
      username: user.username
    })
  }
})
